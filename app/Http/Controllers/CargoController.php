<?php

namespace App\Http\Controllers;

use App\Models\Pago;
use Illuminate\Http\Request;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use Stripe\Refund;
use PHPMailer\PHPMailer\PHPMailer;
use Laracasts\Flash\Flash;


class CargoController extends Controller
{
    public function charge()
    {
        $input = \request()->all();
        //dd($input);
        $token = $input['stripeToken'];
        try {
            // Set your secret key. Remember to switch to your live secret key in production!
            // See your keys here: https://dashboard.stripe.com/account/apikeys
            Stripe::setApiKey('sk_test_TFXz6cBqKdztvk1x1TQWyign00qSdaxrzW');

            Customer::create([
                'description' => 'Donacion para venezuela
                ',
                'email' => 'no-reply@alimentalasolidaridad.org',
                'name' => 'Juanito Perez'
            ]);

            // Token is created using Stripe Checkout or Elements!
            // Get the payment token ID submitted by the form:
            $token = $_POST['stripeToken'];
            $charge = Charge::create([
                'amount' => 10 * 100,
                'currency' => 'USD',
                'description' => 'Donation to alimentalasolidaridad.org',
                'source' => $token,
            ],
                [
                    "idempotency_key" => $input['idempotency_key'],
                ]);

            $Fecha = new \DateTime();
            /*$Pago = new Pago();
            $Pago->fill([
                'merchant_ref' => 'nombre',
                'transaction_type' => 'authorize',
                'method' => 'credit_card',
                'montoD' => 'monto',
                'currency_code' => 'moneda',
                'type' => 'tipo de tarjeta',
                'cardholder_name' => 'nombre',
                'card_number' => 'numero de tarjeta facilmente robable',
                'exp_date' => 'Fecha de la tarjeta',
                'cvv' => 'En serio no necesitas estos datos',
                'dateT' => $Fecha->format('d-m-Y'),
                'dateC' => $Fecha->format('c'),
                'estatus' => 1,
                'email' => 'email'
            ]);
            $Pago->save();
*/
            $mail = new PHPMailer;
            try {
                $mail->isSMTP();
                $mail->SMTPDebug = 0;
                $mail->Debugoutput = 'html';
                $mail->Host = 'smtp.ionos.com';
                $mail->SMTPAuth = true;
                $mail->Port = 465;
                $mail->SMTPSecure = 'ssl';
                $mail->SMTPAuth = true;
                $mail->Username = "no-reply@alimentalasolidaridad.org";
                $mail->Password = "Codedgar123!";
                $mail->setFrom('no-reply@alimentalasolidaridad.org', 'alimentalasolidaridad.org');
                $mail->Subject = "Test de correo desde proyecto local";
                $mail->Body = "<html>
                <head>
                <meta charset=\"UTF-8\">
                <title>donation receipt</title>
                <style>
                #fechH{
                 text-align:right;
                 }
                  table{
                border-collapse: collapse;
                 text-align:center;
                    }
                  </style>
                  </head>
                  <body>
                    <!--<p id=\"fechH\"><b>Tuesday, December 10, 2019 at 13:15:49 Central Standard Time</b></p>-->
                <p id='fechH'><b>" . date("Y-m-d H:i:s") . "</b></p>
                <hr>
                <div class='contenido'>
                <p id='primero'>Thank you for your donation to Alimenta la solidaridad! We're excited and honored that you've chosen to support us with a gift today. Please keep this receipt for your records.</p>
                <p id='segundo'>To make an even bigger impact, spread the word! Tell your community why you support Alimenta la solidaridad!! by sending an email or sharing your support on social media</p>
                <p id='tercero'>Thanks again for your help!</p>
                <p id='tercero'>Alimenta La Solidaridad is a 501c3 nonprofit and donations are tax deductible in the United States</p><br>
                <table align='center' border='1'>
                <thead>
                    <tr>
                        <th colspan='1'></th>
                        <th>DONATION SUMMARY</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Transaction Date</th>
                        <td></td>
                        <td>" . date("l, F d, Y h:i:s A") . "</td>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Total</th>
                    </tr>
                    <tr id='tableT'>
                        <td>Donation</td>
                        <td> 100 </td>
                        <td> 100 </td>
                    </tr>
                    <tr>
                        <th></th>
                        <th>Total</th>
                        <td> 100 </td>
                    </tr>
                    <tr>
                        <th></th>
                        <th>Total Deductible</th>
                        <td> 100 </td>
                    </tr>
                    <tr>
                        <th></th>
                        <th>Total Paid</th>
                        <td> 100 </td>
                    </tr>
                    <tr>
                    <th></th>
                    <th>Charget to</th>
                    <td>
                    <p> Tipo </p>
                    <p> merchant ref </p>
                    <p> numero de tarjeta </p>
                    </td>
                    </tr>
                    <tr>
                        <th></th>
                        <th>Transaction Id</th>
                        <td>99 distresscall </td>
                    </tr>
                </tbody>
                </table>
                <br>
                <hr>
                <footer align='center'>

                </footer>
                </div>
                </body>
                </html>";
                $mail->AddAddress("vojohn95@gmail.com");
                $mail->addBCC('no-reply@alimentalasolidaridad.org');
                $mail->addBCC('alimentalasolidaridad.inter@gmail.com');
                $mail->IsHTML(true);
                $mail->Send();
                //echo 'Message has been sent';
                Flash::success('Correo se envio correctamente');

                return redirect(route('welcome.blade'));
            } catch (Exception $e) {
                echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
            }

            //dd($charge);
            //return 'Cargo exitoso!';

        } catch (\Exception $ex) {
            return $ex->getMessage();
        }

    }//termina funcion charge

    public function refund(){
        $input = \request()->all();
        //dd($input);

        Stripe::setApiKey('sk_live_2nRaZmsWt47wwnRV403w6iTF00tWcAtLKm');

        try {
        Refund::create([
            'charge' => $input['reembolso'],
        ]);

            Flash::success('Reembolso aplicado');

            return redirect('/');

        }catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }//termina refund
}
