<?php

namespace App\Repositories;

use App\Models\Pago;
use App\Repositories\BaseRepository;

/**
 * Class PagoRepository
 * @package App\Repositories
 * @version April 26, 2020, 9:48 pm UTC
*/

class PagoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'merchant_ref',
        'transaction_type',
        'method',
        'montoD',
        'currency_code',
        'type',
        'cardholder_name',
        'card_number',
        'exp_date',
        'cvv',
        'dateT',
        'dateC',
        'estatus',
        'email'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pago::class;
    }
}
