<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Pago
 * @package App\Models
 * @version April 26, 2020, 9:48 pm UTC
 *
 * @property string $merchant_ref
 * @property string $transaction_type
 * @property string $method
 * @property string $montoD
 * @property string $currency_code
 * @property string $type
 * @property string $cardholder_name
 * @property string $card_number
 * @property string $exp_date
 * @property string $cvv
 * @property string $dateT
 * @property string $dateC
 * @property boolean $estatus
 * @property string $email
 */
class Pago extends Model
{

    public $table = 'table_payeezy';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'merchant_ref',
        'transaction_type',
        'method',
        'montoD',
        'currency_code',
        'type',
        'cardholder_name',
        'card_number',
        'exp_date',
        'cvv',
        'dateT',
        'dateC',
        'estatus',
        'email'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'merchant_ref' => 'string',
        'transaction_type' => 'string',
        'method' => 'string',
        'montoD' => 'string',
        'currency_code' => 'string',
        'type' => 'string',
        'cardholder_name' => 'string',
        'card_number' => 'string',
        'exp_date' => 'string',
        'cvv' => 'string',
        'dateT' => 'string',
        'dateC' => 'string',
        'estatus' => 'boolean',
        'email' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'merchant_ref' => 'required',
        'transaction_type' => 'required',
        'method' => 'required',
        'montoD' => 'required',
        'currency_code' => 'required',
        'type' => 'required',
        'cardholder_name' => 'required',
        'card_number' => 'required',
        'exp_date' => 'required',
        'cvv' => 'required',
        'dateT' => 'required',
        'dateC' => 'required',
        'estatus' => 'required',
        'email' => 'required'
    ];

    
}
