<!DOCTYPE html>
<html>
<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
<div class="container">
    @include('flash::message')
    <br>

    <form action="{{ url('stripe') }}" method="post" id="payment-form">
        @csrf
        <div class="form-row">
            <label for="card-element">
                Credit or debit card
            </label>

            <div id="card-element">
                <!-- A Stripe Element will be inserted here. -->
            </div>

            <!-- Used to display Element errors. -->
            <div id="card-errors" role="alert"></div>
        </div>
        <br>
        <button class="waves-effect waves-light btn">Submit Payment</button>
    </form>

    <hr>

    <form action="{{ url('refund') }}" method="post">
        @csrf
        <div class="form-row">
            <label for="card-element">
                ID reembolso
            </label>

            <div id="card-element">
                <!-- A Stripe Element will be inserted here. -->
                <div class="input-field col s6">
                    <input placeholder="ID reembolso" id="first_name" type="text" class="validate" name="reembolso">
                </div>
            </div>
        </div>
        <br>
        <button type="submit" class="waves-effect waves-light btn">Solicitar reembolso</button>
    </form>

</div>

<!--JavaScript at end of body for optimized loading-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script src="https://js.stripe.com/v3/"></script>
<script>
    // Set your publishable key: remember to change this to your live publishable key in production
    // See your keys here: https://dashboard.stripe.com/account/apikeys
    //var stripe = Stripe('pk_test_IfAWcL0KMKhTxCJCcSmzYAuS007SjmA2Ct');
    //Key produccion
    var stripe = Stripe('pk_live_Y3NsPOA44uPdGoZginEqwkLA00Tn96A6AY')
    var elements = stripe.elements();

    // Custom styling can be passed to options when creating an Element.
    var style = {
        base: {
            // Add your base input styles here. For example:
            fontSize: '16px',
            color: '#32325d',
        },
    };

    // Create an instance of the card Element.
    var card = elements.create('card', {style: style});

    // Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

    // Create a token or display an error when the form is submitted.
    var form = document.getElementById('payment-form');
    form.addEventListener('submit', function (event) {
        event.preventDefault();

        stripe.createToken(card).then(function (result) {
            if (result.error) {
                // Inform the customer that there was an error.
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
            } else {
                // Send the token to your server.
                stripeTokenHandler(result.token);
            }
        });
    });

    function stripeTokenHandler(token) {
        var uuid = uuidv4();
        console.log("String de javascript: " + uuid);

        // Insert the token ID into the form so it gets submitted to the server
        var form = document.getElementById('payment-form');
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);
        var hiddenInput2 = document.createElement('input');
        hiddenInput2.setAttribute('type', 'hidden');
        hiddenInput2.setAttribute('name', 'idempotency_key');
        hiddenInput2.setAttribute('value', uuid);
        form.appendChild(hiddenInput2);

        // Submit the form

        alert(uuid);
        form.submit();
    }

    function uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    //console.log("String de javascript: " + uuidv4());


</script>
</body>
</html>

