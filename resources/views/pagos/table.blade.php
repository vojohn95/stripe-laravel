<div class="table-responsive">
    <table class="table" id="pagos-table">
        <thead>
            <tr>
                <th>Merchant Ref</th>
        <th>Transaction Type</th>
        <th>Method</th>
        <th>Montod</th>
        <th>Currency Code</th>
        <th>Type</th>
        <th>Cardholder Name</th>
        <th>Card Number</th>
        <th>Exp Date</th>
        <th>Cvv</th>
        <th>Datet</th>
        <th>Datec</th>
        <th>Estatus</th>
        <th>Email</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($pagos as $pago)
            <tr>
                <td>{{ $pago->merchant_ref }}</td>
            <td>{{ $pago->transaction_type }}</td>
            <td>{{ $pago->method }}</td>
            <td>{{ $pago->montoD }}</td>
            <td>{{ $pago->currency_code }}</td>
            <td>{{ $pago->type }}</td>
            <td>{{ $pago->cardholder_name }}</td>
            <td>{{ $pago->card_number }}</td>
            <td>{{ $pago->exp_date }}</td>
            <td>{{ $pago->cvv }}</td>
            <td>{{ $pago->dateT }}</td>
            <td>{{ $pago->dateC }}</td>
            <td>{{ $pago->estatus }}</td>
            <td>{{ $pago->email }}</td>
                <td>
                    {!! Form::open(['route' => ['pagos.destroy', $pago->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('pagos.show', [$pago->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('pagos.edit', [$pago->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
