<!-- Merchant Ref Field -->
<div class="form-group">
    {!! Form::label('merchant_ref', 'Merchant Ref:') !!}
    <p>{{ $pago->merchant_ref }}</p>
</div>

<!-- Transaction Type Field -->
<div class="form-group">
    {!! Form::label('transaction_type', 'Transaction Type:') !!}
    <p>{{ $pago->transaction_type }}</p>
</div>

<!-- Method Field -->
<div class="form-group">
    {!! Form::label('method', 'Method:') !!}
    <p>{{ $pago->method }}</p>
</div>

<!-- Montod Field -->
<div class="form-group">
    {!! Form::label('montoD', 'Montod:') !!}
    <p>{{ $pago->montoD }}</p>
</div>

<!-- Currency Code Field -->
<div class="form-group">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    <p>{{ $pago->currency_code }}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{{ $pago->type }}</p>
</div>

<!-- Cardholder Name Field -->
<div class="form-group">
    {!! Form::label('cardholder_name', 'Cardholder Name:') !!}
    <p>{{ $pago->cardholder_name }}</p>
</div>

<!-- Card Number Field -->
<div class="form-group">
    {!! Form::label('card_number', 'Card Number:') !!}
    <p>{{ $pago->card_number }}</p>
</div>

<!-- Exp Date Field -->
<div class="form-group">
    {!! Form::label('exp_date', 'Exp Date:') !!}
    <p>{{ $pago->exp_date }}</p>
</div>

<!-- Cvv Field -->
<div class="form-group">
    {!! Form::label('cvv', 'Cvv:') !!}
    <p>{{ $pago->cvv }}</p>
</div>

<!-- Datet Field -->
<div class="form-group">
    {!! Form::label('dateT', 'Datet:') !!}
    <p>{{ $pago->dateT }}</p>
</div>

<!-- Datec Field -->
<div class="form-group">
    {!! Form::label('dateC', 'Datec:') !!}
    <p>{{ $pago->dateC }}</p>
</div>

<!-- Estatus Field -->
<div class="form-group">
    {!! Form::label('estatus', 'Estatus:') !!}
    <p>{{ $pago->estatus }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $pago->email }}</p>
</div>

