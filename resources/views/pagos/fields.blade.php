<!-- Merchant Ref Field -->
<div class="form-group col-sm-6">
    {!! Form::label('merchant_ref', 'Merchant Ref:') !!}
    {!! Form::text('merchant_ref', null, ['class' => 'form-control']) !!}
</div>

<!-- Transaction Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('transaction_type', 'Transaction Type:') !!}
    {!! Form::text('transaction_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Method Field -->
<div class="form-group col-sm-6">
    {!! Form::label('method', 'Method:') !!}
    {!! Form::text('method', null, ['class' => 'form-control']) !!}
</div>

<!-- Montod Field -->
<div class="form-group col-sm-6">
    {!! Form::label('montoD', 'Montod:') !!}
    {!! Form::text('montoD', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    {!! Form::text('currency_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::text('type', null, ['class' => 'form-control']) !!}
</div>

<!-- Cardholder Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cardholder_name', 'Cardholder Name:') !!}
    {!! Form::text('cardholder_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Card Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('card_number', 'Card Number:') !!}
    {!! Form::text('card_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Exp Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('exp_date', 'Exp Date:') !!}
    {!! Form::text('exp_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Cvv Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cvv', 'Cvv:') !!}
    {!! Form::text('cvv', null, ['class' => 'form-control']) !!}
</div>

<!-- Datet Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dateT', 'Datet:') !!}
    {!! Form::text('dateT', null, ['class' => 'form-control']) !!}
</div>

<!-- Datec Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dateC', 'Datec:') !!}
    {!! Form::text('dateC', null, ['class' => 'form-control']) !!}
</div>

<!-- Estatus Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estatus', 'Estatus:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('estatus', 0) !!}
        {!! Form::checkbox('estatus', '1', null) !!}
    </label>
</div>


<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('pagos.index') }}" class="btn btn-default">Cancel</a>
</div>
