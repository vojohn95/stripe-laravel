<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>donation receipt</title>
        <style>
        #fechH{
            text-align:right;
        }
        table{
            border-collapse: collapse;
            text-align:center;
        }
        </style>
    </head>
    <body>
        <!--<p id="fechH"><b>Tuesday, December 10, 2019 at 13:15:49 Central Standard Time</b></p>-->
        <p id="fechH"><b>{{ date('Y-m-d H:i:s') }}</b></p>
        <hr>
        <div class="contenido">
            <p id="primero">Thank you for your donation to Emma's Torch!. We're excited and honored that you've chosen to support us with a gift today. Please keep this receipt for your records.</p>
            <p id="segundo">To make an even bigger impact, spread the word! Tell your community why you support Emma's Torch! by sending an email or sharing your support on social media</p>
            <p id="tercero">If you have any questions, feel free to contact us at info@emmastorch.org.</p>
            <p id="tercero">Thanks again for your help!</p><br>
            <table align="center" border="1" >
                <thead>
                    <tr>
                        <th colspan="1"></th>
                        <th>DONATION SUMMARY</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Transaction Date</th>
                        <td></td>
                        <td>" . <?= date("Y-m-d H:i:s"); ?> ."</td>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Total</th>
                    </tr>
                    <tr id="tableT">
                        <td>Donation</td>
                        <td>${{$distressCall->montoD}}</td>
                        <td>${{$distressCall->montoD}}</td>
                    </tr>
                    <tr>
                        <th></th>
                        <th>Total</th>
                        <td>${{$distressCall->montoD}}</td>
                    </tr>
                    <tr>
                        <th></th>
                        <th>Total Deductible</th>
                        <td>${{$distressCall->montoD}}</td>
                    </tr>
                    <tr>
                        <th></th>
                        <th>Total Paid</th>
                        <td>${{$distressCall->montoD}}</td>
                    </tr>
                    <tr>
                        <th></th>
                        <th>Charged To</th>
                        <td>
                            <p>{{$distressCall->type}}</p><br>
                            <p>{{$distressCall->merchant_ref}}</p>
                            <p>{{$distressCall->card_number}}</p>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <th>Transaction Id</th>
                        <td>99{{$distressCall->card_number}}49163</td>
                    </tr>
                </tbody>
            </table><br>
            <hr>
        <footer align='center' >
            <p><b>Contact Us</b><br>Emma's Torch<br>345 Smith Street<br>Brooklyn, New York 11231<br>718-243-1222<br>info@emmastorch.org</p>
        </footer>
        </div>
    </body>
</html>
